<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Slider extends Model
{
    public function images()
    {
        return $this->belongsToMany('App\Image', 'slider_item', 'slider_id', 'image_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'slider_item', 'slider_id', 'product_id');
    }
}
