<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = [
        'name',
        'set_number',
        'new_price',
        'measure',
        'category_id',
        'short_description',
        'long_description',
        'image'
    ];

    public function getBadges()
    {
        $badges = [];
        $date = Carbon::now();
        if ($date->diffInDays($this->created_at) < 5) $badges['new'] = 'Новинка';
        if ($this->views > 100) $badges['hot'] = 'Популярное';
        if ($this->old_price !== null) $badges['sale'] = 'Скидка';

        return $badges;
    }

    public function hasBetterPrice()
    {
        return $this->old_price == null;
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function ancestors()
    {
        $start = $this->category;
        $start->link = "/category/$start->id";
        $ancestors = [$start];
        while (true) {
            $category = $start;
            if ($category->parent_id == null) {
                return array_reverse($ancestors);
            } else {
                $start = $category->parent;
                $start->link = "/category/$start->id";
                $ancestors[] = $start;
            }
        }
    }
}
