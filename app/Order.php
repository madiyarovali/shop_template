<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'order_note',
        'status',
        'with_delivery',
        'total',
        'delivery_address',
        'user_id'
    ];

    protected $dates = ['created_at'];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }
}
