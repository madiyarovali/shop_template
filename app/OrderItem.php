<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{
    protected $fillable = [
        'name',
        'price',
        'quantity',
        'product_id',
        'order_id'
    ];
}
