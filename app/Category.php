<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Category extends Model
{
    protected $fillable = [
        'name',
        'parent_id',
        'image'
    ];

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'category_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function ancestors()
    {
        $start = $this->parent;
        if ($start != null) {
            $start->link = "/category/$start->id";
        }
        $ancestors = [$start];
        while (true) {
            $category = $start;
            if ($category == null) {
                return array_filter(array_reverse($ancestors));
            } else {
                $start = $category->parent;
                if ($start != null) {
                    $start->link = "/category/$start->id";
                }
                $ancestors[] = $start;
            }
        }
    }

    public function descendants()
    {
        $categories = new Collection();

        foreach ($this->children as $child) {
            $categories->push($child);
            $categories = $categories->merge($child->descendants());
        }

        return $categories;
    }
}
