<?php

namespace App\Http\Controllers;

use App\Http\Requests\{ AddCartRequest, UpdateCartRequest, RemoveCartRequest };
use Illuminate\Http\Request;
use App\Http\Services\CartService;
use App\{ Product };
use Cart;

class CartController extends Controller
{
    /* 
     * Request
     * @param `product_id` integer required
     * @param `qty` integer required
     * @param `options` array nullable
     * 
     * @return array with cart view, total and count
     */
    public function add(AddCartRequest $request)
    {
        $product = Product::findOrFail($request->product_id);
        $price = $product->new_price;
        $options = $request->options ?? [];
        $qty = $request->qty;
        Cart::instance('cart')->add([
            'id'        => $product->id,
            'name'      => $product->name,
            'qty'       => $qty,
            'price'     => $price,
            'options'   => $options
        ])->associate('\App\Product');

        $total = Cart::instance('cart')->total();
        $count = Cart::instance('cart')->count();
        return [
            'cart_list'  => view(cart_view())->render(),
            'checkout_list' => view(checkout_view())->render(),
            'total' => $total,
            'count' => $count
        ];
    }

    /* 
     * Request
     * @param `row_id` string required
     * @param `qty` integer required
     * 
     * @return array with cart view, total and count
     */
    public function update(UpdateCartRequest $request)
    {
        $row_id = $request->row_id;
        $qty = $request->qty;
        Cart::instance('cart')->update($row_id, $qty);

        $total = Cart::instance('cart')->total();
        $count = Cart::instance('cart')->count();
        return [
            'cart_list'  => view(cart_view())->render(),
            'checkout_list' => view(checkout_view())->render(),
            'total' => $total,
            'count' => $count
        ];
    }

    /* 
     * Request
     * @param `row_id` string required
     * 
     * @return array with cart view, total and count
     */
    public function remove(RemoveCartRequest $request)
    {   
        $row_id = $request->row_id;
        Cart::instance('cart')->remove($row_id);

        $total = Cart::instance('cart')->total();
        $count = Cart::instance('cart')->count();
        return [
            'cart_list'  => view(cart_view())->render(),
            'checkout_list' => view(checkout_view())->render(),
            'total' => $total,
            'count' => $count
        ];
    }
}
