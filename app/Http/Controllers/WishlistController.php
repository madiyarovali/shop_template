<?php

namespace App\Http\Controllers;

use App\Http\Requests\{ AddWishlistRequest, RemoveWishlistRequest };
use Illuminate\Http\Request;
use App\Http\Services\CartService;
use App\{ Product };
use Cart;

class WishlistController extends Controller
{
    /* 
     * Request
     * @param `product_id` integer required
     * @param `qty` integer required
     * @param `options` array nullable
     * 
     * @return array with cart view, total and count
     */
    public function add(AddWishlistRequest $request)
    {
        $product = Product::findOrFail($request->product_id);
        $price = $product->new_price;
        $options = $request->options ?? [];
        $qty = $request->qty;
        Cart::instance('wishlist')->add([
            'id'        => $product->id,
            'name'      => $product->name,
            'qty'       => $qty,
            'price'     => $price,
            'options'   => $options
        ])->associate('\App\Product');

        $total = Cart::instance('wishlist')->total();
        $count = Cart::instance('wishlist')->count();
        return [
            'wishlist_list'  => view(wishlist_view())->render(),
            'count' => $count
        ];
    }

    /* 
     * Request
     * @param `row_id` string required
     * 
     * @return array with cart view, total and count
     */
    public function remove(RemoveWishlistRequest $request)
    {   
        $row_id = $request->row_id;
        Cart::instance('wishlist')->remove($row_id);

        $total = Cart::instance('wishlist')->total();
        $count = Cart::instance('wishlist')->count();
        return [
            'wishlist_list'  => view(wishlist_view())->render(),
            'count' => $count
        ];
    }
}