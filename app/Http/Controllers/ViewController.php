<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{ Category, Image, Product, Page, Slider };

class ViewController extends Controller
{
    public function index()
    {
        $slider = Slider::where('key', 'main_slider')->first();
        $product_sliders = Slider::where('key', 'product_sliders')->get();
        $bestsellers = Slider::where('key', 'bestsellers')->first();
        $banner = Image::where('name', 'banner-1')->first();
        $brands = Slider::where('key', 'brands')->first();
        return view('index', compact('slider', 'product_sliders', 'banner', 'bestsellers', 'brands'));
    }

    public function checkout()
    {
        $options = [
            'Город Нур-Султан' => 1,
            'Город Алматы' => 1,
            'Город Шымкент' => 1,
            'Акмолинская область' => 1,
            'Актюбинская область' => 1,
            'Алматинская область' => 1,
            'Атырауская область' => 1,
            'Восточно-Казахстанская область' => 1,
            'Жамбылская область' => 1,
            'Западно-Казахстанская область' => 1,
            'Карагандинская область' => 1,
            'Костанайская область' => 1,
            'Кызылординская область' => 1,
            'Мангистауская область' => 1,
            'Павлодарская область' => 1,
            'Северо-Казахстанская область' => 1,
            'Туркестанская область' => 1
        ];
        return view('checkout', compact('options'));
    }

    public function product($id)
    {
        $product = Product::findOrFail($id);
        $reviews = [];
        return view('product', compact('product', 'reviews'));
    }

    public function category(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $ids = $category->descendants()->pluck('id');
        $ids[] = $id;
        $per_page = $request->per_page ?? 12;
        $sort = $request->sort ?? 'new_price';
        $order = $request->order ?? 'ASC';
        $products = Product::whereIn('category_id', $ids)->orderBy($sort, $order)->paginate($per_page);

        return view('category', compact('category', 'products', 'sort', 'order'));
    }

    public function search(Request $request)
    {
        $q = $request->search;
        $per_page = $request->per_page ?? 12;
        $sort = $request->sort ?? 'new_price';
        $order = $request->order ?? 'ASC';
        $products = Product::where('name', 'like', "%$q%")->orderBy($sort, $order)->paginate($per_page);

        return view('search', compact('products', 'q', 'sort', 'order'));
    }

    public function page($url)
    {
        $page = Page::where('url', $url)->first();
        return view('pages', compact('page'));
    }
}
