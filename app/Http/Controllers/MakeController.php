<?php

namespace App\Http\Controllers;

use DB, Mail, Cart, SimpleXLS;
use App\{ Order, OrderItem, Product };
use Illuminate\Http\Request;
use App\Http\Services\{ OrderService, ShopService };

class MakeController extends Controller
{
    public function order(Request $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->all();
            $prepared_order = OrderService::prepareOrder($data);
            $order = Order::create($prepared_order);
            $prepared_items = OrderService::prepareItems($order);
            $order_items = [];
            foreach ($prepared_items as $prepared_item) {
                $order_items[] = OrderItem::create($prepared_item);
            }
            $text = OrderService::generateMessage($order, $order_items);
            Cart::instance('cart')->destroy();
            Mail::raw($text, function ($message){
                $message->to(setting('admin.mail_receiver'));
                $message->subject('Новая заявка');
            });
        });
        
        return redirect('/')->with('message', 'Спасибо за заказ. Мы с Вами скоро свяжемся');
    }

    public function request(Request $request)
    {
        //
    }

    public function import()
    {
        DB::transaction(function () {
            $path = 'storage/files/price.xls';
            if ( $xlsx = SimpleXLS::parse($path) ) {
                echo '<table border="1" cellpadding="3" style="border-collapse: collapse">';
                $service = new ShopService;
                $service->excel($xlsx->rows());
                foreach( $xlsx->rows() as $index => $r ) {
                    echo '<tr><td>'.implode('</td><td>', $r ).'</td></tr>';
                }
                echo '</table>';
            } else {
                echo SimpleXLS::parseError();
            } 
        });
    }

    public function parsing()
    {
        // DB::transaction(function () {
            $service = new ShopService;
            $service->parse(); 
        // });
        dd(Product::count());
    }
}
