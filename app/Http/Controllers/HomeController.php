<?php

namespace App\Http\Controllers;

use App\{ Address, Order, OrderItem, User };
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $user = request()->user();
        $default_address = $user->addresses()->where('is_default', 1)->get();
        $recently_orders = Order::where('user_id', $user->id)->orderBy('id', 'DESC')->limit(3)->get();
        return view('account.account-dashboard', compact('user', 'default_address', 'recently_orders'));
    }

    public function profile()
    {
        $user = request()->user();
        return view('account.account-profile', compact('user'));
    }

    public function orders()
    {
        $user = request()->user();
        $orders = Order::where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(10);
        return view('account.account-orders', compact('orders', 'user'));
    }

    public function addresses()
    {
        $user = request()->user();
        $addresses = Address::where('user_id', $user->id)->get();
        return view('account.account-addresses', compact('user', 'addresses'));
    }

    public function password()
    {
        return view('account.account-password');
    }

    public function addAddress(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'country' => 'required',
            'phone' => 'required',
        ]);
        $user = $request->user();
        $data = $request->except(['_token']);
        $data['is_default'] = isset($data['is_default']);
        $data['user_id'] = $user->id;
        Address::where('user_id', $user->id)->update([
            'is_default' => false
        ]);
        Address::create($data);
        return back()->with('message', 'Информация добавлена');
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'avatar' => 'nullable|image|max:5120'
        ]);
        $data = $request->except(['_token']);
        $user = $request->user();
        if ($request->has('avatar')) {
            $data['avatar'] = $request->file('avatar')->store('/public/users');
            $data['avatar'] = substr($data['avatar'], 7);
        } else {
            $data['avatar'] = $user->avatar;
        }
        User::where('id', $user->id)->update($data);
        return back()->with('message', 'Информация обновлена');
    }

    public function updateAddress(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'country' => 'required',
            'phone' => 'required',
        ]);
        $user = $request->user();
        $data = $request->except(['_token']);
        $data['is_default'] = isset($data['is_default']);
        $data['user_id'] = $user->id;
        Address::where('user_id', $user->id)->update([
            'is_default' => false
        ]);
        Address::where('user_id', $user->id)->where('id', $data['id'])->update($data);
        return back()->with('message', 'Информация обновлена');
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed|min:8'
        ]);
        $user = request()->user();
        $data = $request->except(['_token']);
        if (\Hash::check($data['current_password'], $user->password)) {
            $user->password = bcrypt($data['password']);
            $user->save();
        }

        return back()->with('message', 'Пароль сброшен');
    }
}
