<?php

use App\StoreSetting;
use App\Http\Services\ShopService;

if (! function_exists('cart_view')) {
    function cart_view () {
        $cart_view = StoreSetting::where('key', 'cart_view')->first();
        return strip_tags($cart_view->value);
    }
}

if (! function_exists('wishlist_view')) {
    function wishlist_view () {
        $wishlist_view = StoreSetting::where('key', 'wishlist_view')->first();
        return strip_tags($wishlist_view->value);
    }
}

if (! function_exists('checkout_view')) {
    function checkout_view () {
        $checkout_view = StoreSetting::where('key', 'checkout_view')->first();
        return strip_tags($checkout_view->value);
    }
}

if (! function_exists('add_cart_text')) {
    function add_cart_text () {
        $text = StoreSetting::where('key', 'add_cart_text')->first();
        return $text->value;
    }
}

if (! function_exists('update_cart_text')) {
    function update_cart_text () {
        $text = StoreSetting::where('key', 'update_cart_text')->first();
        return $text->value;
    }
}

if (! function_exists('delete_cart_text')) {
    function delete_cart_text () {
        $text = StoreSetting::where('key', 'delete_cart_text')->first();
        return $text->value;
    }
}

if (! function_exists('add_wishlist_text')) {
    function add_wishlist_text () {
        $text = StoreSetting::where('key', 'add_wishlist_text')->first();
        return $text->value;
    }
}

if (! function_exists('delete_wishlist_text')) {
    function delete_wishlist_text () {
        $text = StoreSetting::where('key', 'delete_wishlist_text')->first();
        return $text->value;
    }
}

if (! function_exists('v_setting')) {
    function v_setting ($data) {
        $servername = env('DB_HOST', '127.0.0.1');
        $username = env('DB_USERNAME', 'forge');
        $password = env('DB_PASSWORD', '');
        $dbname = env('DB_DATABASE', 'forge');
        $socket = env('DB_SOCKET');
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname, $socket);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT value FROM setttings WHERE key = $data";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            $conn->close();
            $row = $result->fetch_assoc();
            return $row['value'];
        } else {
            // echo "0 results";
        }
        return;
    }
}