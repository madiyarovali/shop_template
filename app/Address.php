<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Address extends Model
{
    protected $fillable = [
        'name',
        'city',
        'country',
        'address',
        'phone',
        'email',
        'is_default',
        'user_id'
    ];
}
