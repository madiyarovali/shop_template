<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ViewController@index');
Route::get('search', 'ViewController@search');
Route::get('checkout', 'ViewController@checkout');
Route::get('product/{id}', 'ViewController@product');
Route::get('category/{id}', 'ViewController@category');
Route::get('page/{url}', 'ViewController@page');

Route::group(['prefix' => 'account'], function () {
    Route::get('dashboard', 'HomeController@dashboard');
    Route::get('profile', 'HomeController@profile');
    Route::get('orders', 'HomeController@orders');
    Route::get('addresses', 'HomeController@addresses');
    Route::get('password', 'HomeController@password');

    Route::post('update/profile', 'HomeController@updateProfile');
    Route::post('add/address', 'HomeController@addAddress');
    Route::post('update/address', 'HomeController@updateAddress');
    Route::post('reset/password', 'HomeController@resetPassword');
});

Route::group(['prefix' => 'make'], function () {
    Route::post('order', 'MakeController@order');
    Route::post('import', 'MakeController@import');
    Route::post('request', 'MakeController@request');
    Route::post('parsing', 'MakeController@parsing');
});

Route::group(['prefix' => 'cart'], function () {
    Route::post('add', 'CartController@add');
    Route::post('update', 'CartController@update');
    Route::post('remove', 'CartController@remove');
    Route::post('destroy', 'CartController@destroy');
});

Route::group(['prefix' => 'wishlist'], function () {
    Route::post('add', 'WishlistController@add');
    Route::post('remove', 'WishlistController@remove');
    Route::post('destroy', 'WishlistController@destroy');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();