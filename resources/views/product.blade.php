@extends('layouts.app') @section('content')
<div class="site__body">
    @include('partials.breadcrumbs', ['name' => $product->name, 'ancestors' => $product->ancestors()])
    <div class="block">
        <div class="container">
            <div class="product product--layout--standard" data-layout="standard">
                <div class="product__content">
                    <!-- .product__gallery -->
                    <div class="product__gallery">
                        <div class="product-gallery">
                            <div class="product-gallery__featured">
                                <button class="product-gallery__zoom">
                                    <svg width="24px" height="24px">
                                        <use xlink:href="{{ asset('images/sprite.svg#zoom-in-24') }}"></use>
                                    </svg>
                                </button>
                                <div class="owl-carousel" id="product-image">

                                    <div class="product-image product-image--location--gallery">
                                        <a href="{{ asset("storage/$product->image") }}" data-width="700" data-height="700" class="product-image__body" target="_blank">
                                          <img class="product-image__img" src="{{ asset("storage/$product->image") }}" alt="">
                                       </a>
                                    </div>

                                </div>
                            </div>
                            <div class="product-gallery__carousel">
                                <div class="owl-carousel" id="product-carousel">

                                    <a href="{{ asset("storage/$product->image") }}" class="product-image product-gallery__carousel-item">
                                        <div class="product-image__body"><img class="product-image__img product-gallery__carousel-image" src="{{ asset("storage/$product->image") }}" alt=""></div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .product__gallery / end -->
                    <!-- .product__info -->
                    <div class="product__info">
                        <div class="product__wishlist-compare">
                            <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Wishlist">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('images/sprite.svg#wishlist-16') }}"></use>
                                </svg>
                            </button>
                            <button type="button" class="btn btn-sm btn-light btn-svg-icon" data-toggle="tooltip" data-placement="right" title="Compare">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('images/sprite.svg#compare-16') }}"></use>
                                </svg>
                            </button>
                        </div>
                        <h1 class="product__name">{{ $product->name }}</h1>

                        <div class="product__description">
                            {!! $product->short_description !!}
                        </div>
                        <ul class="product__meta">
                            <li class="product__meta-availability">
                                @if ($product->is_active)
                                <span class="text-success">Есть на складе</span> @else
                                <span class="text-danger">Нет на складе</span> @endif
                            </li>
                            <li>ID: {{ $product->id }}</li>
                        </ul>
                    </div>
                    <!-- .product__info / end -->
                    <!-- .product__sidebar -->
                    <div class="product__sidebar">
                        <div class="product__availability">
                            @if ($product->is_active)
                            <span class="text-success">Есть на складе</span> @else
                            <span class="text-danger">Нет на складе</span> @endif
                        </div>
                        <div class="product__prices">
                            @if ($product->old_price !== null)
                            <span class="product-card__new-price">
                                {{ $product->new_price }} ₸
                            </span> 
                            <span class="product-card__old-price">
                                {{ $product->old_price }} ₸
                            </span>
                            @else
                                {{ $product->new_price }} ₸
                            @endif
                        </div>
                        <!-- .product__options -->
                        <form class="product__options">
                            <div class="form-group product__option">
                                <label class="product__option-label" for="product-quantity">Количество</label>
                                <div class="product__actions">
                                    <div class="product__actions-item">
                                        <div class="input-number product__quantity">
                                            <input id="product-quantity" class="input-number__input form-control form-control-lg" type="number" min="1" value="1" data-target="#product-adder-cart">
                                            <div class="input-number__add"></div>
                                            <div class="input-number__sub"></div>
                                        </div>
                                    </div>
                                    <div class="product__actions-item product__actions-item--addtocart">
                                        <button
                                            id="product-adder-cart"
                                            class="btn btn-primary btn-lg add-cart"
                                            product-id="{{ $product->id }}"
                                            qty="1">
                                            Добавить в корзину
                                        </button>
                                    </div>
                                    <div class="product__actions-item product__actions-item--wishlist">
                                        <button
                                            qty="1"
                                            product-id="{{ $product->id }}"
                                            type="button"
                                            class="btn btn-secondary btn-svg-icon btn-lg add-wishlist"
                                            data-toggle="tooltip"
                                            title="Wishlist">
                                            <svg width="16px" height="16px">
                                                <use xlink:href="{{ asset('images/sprite.svg#wishlist-16') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- .product__options / end -->
                    </div>
                    <!-- .product__end -->
                </div>
            </div>
            <div class="product-tabs product-tabs--sticky">
                <div class="product-tabs__list">
                    <div class="product-tabs__list-body">
                        <div class="product-tabs__list-container container">
                            <a href="#tab-description" class="product-tabs__item product-tabs__item--active">Полное описание</a> 
                            <a href="#tab-reviews" class="product-tabs__item">Отзывы</a></div>
                    </div>
                </div>
                <div class="product-tabs__content">
                    <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                        <div class="typography">
                            {!! $product->long_description !!}
                        </div>
                    </div>
                    <div class="product-tabs__pane" id="tab-reviews">
                        <div class="reviews-view">
                            <div class="reviews-view__list">
                                <h3 class="reviews-view__header">Отзывы пользователей</h3>
                                <div class="reviews-list">
                                    <ol class="reviews-list__content">
                                        @foreach($reviews as $review)
                                        <li class="reviews-list__item">
                                            <div class="review">
                                                <div class="review__avatar"><img src="{{ asset("storage/$review->avatar") }}" alt=""></div>
                                                <div class="review__content">
                                                    <div class="review__author">{{ $review->name }}</div>
                                                    <div class="review__rating">
                                                        <div class="rating">
                                                            <div class="rating__body">
                                                                @for ($i = 0; $i < $review->rating; $i++)
                                                                <svg class="rating__star rating__star--active" width="13px" height="12px">
                                                                    <g class="rating__fill">
                                                                        <use xlink:href="{{ asset('images/sprite.svg#star-normal') }}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use xlink:href="{{ asset('images/sprite.svg#star-normal-stroke') }}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div class="rating__star rating__star--only-edge rating__star--active">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                @endfor

                                                                @for ($i = $review->rating - 1; $i < 5; $i++)
                                                                <svg class="rating__star" width="13px" height="12px">
                                                                    <g class="rating__fill">
                                                                        <use xlink:href="{{ asset('images/sprite.svg#star-normal') }}"></use>
                                                                    </g>
                                                                    <g class="rating__stroke">
                                                                        <use xlink:href="{{ asset('images/sprite.svg#star-normal-stroke') }}"></use>
                                                                    </g>
                                                                </svg>
                                                                <div class="rating__star rating__star--only-edge">
                                                                    <div class="rating__fill">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                    <div class="rating__stroke">
                                                                        <div class="fake-svg-icon"></div>
                                                                    </div>
                                                                </div>
                                                                @endfor
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="review__text">{{ $review->text }}</div>
                                                    <div class="review__date">{{ $review->created_at }}</div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @php
    $id = $product->category_id;
    $slider = \App\Category::findOrFail($id);
    $slider->name = 'Похожие продукты';
    @endphp
    @include('partials.product_slider', ['slider' => $slider])
</div>
<!-- site__body / end -->
<!-- site__footer -->
@endsection