@php
$fix_menu = 1;
@endphp

@extends('layouts.app')
@section('content')
<div class="site__body">
    @include('partials.slider')
    @include('partials.advantages')

    <div id="featured"></div>
    @foreach ($product_sliders as $slider)
        @include('partials.product_slider', ['slider' => $slider])
    @endforeach
    @include('partials.banner', ['banner' => $banner])
    @include('partials.bestsellers', ['slider' => $bestsellers])
    @include('partials.brands', ['slider' => $brands])
</div>
@endsection