@foreach(Cart::instance('cart')->content() as $item)
<tr>
    <td>{{ $item->name }} × {{ $item->qty }} {{ $item->model->measure }}</td>
    <td>{{ $item->subtotal }} ₸</td>
</tr>
@endforeach