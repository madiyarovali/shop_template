<div class="block-slideshow block-slideshow--layout--with-departments block">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 d-none d-lg-block"></div>
            <div class="col-12 col-lg-9">
                <div class="block-slideshow__body">
                    <div class="owl-carousel">
                        @foreach ($slider->images as $image)
                            <a class="block-slideshow__slide" href="{{ $image->link_href }}">
                                <div
                                    class="block-slideshow__slide-image block-slideshow__slide-image--desktop"
                                    style="background-image: url({{ asset("storage/$image->full_path") }})">
                                        
                                </div>
                                <div
                                    class="block-slideshow__slide-image block-slideshow__slide-image--mobile"
                                    style="background-image: url(({{ asset("storage/$image->mobile_path") }}))">
    
                                </div>
                                <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">{{ $image->title }}</div>
                                    <div class="block-slideshow__slide-text">{{ $image->subtitle }}</div>
                                    <div class="block-slideshow__slide-button">
                                        <span class="btn btn-primary btn-lg">
                                            {{ $image->link_text }}
                                        </span>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>