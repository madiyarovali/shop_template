<div class="block block-features block-features--layout--classic" id="advantages">
    <div class="container">
        <div class="block-features__list">
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('images/sprite.svg#fi-free-delivery-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Доставка</div>
                    <div class="block-features__subtitle">При заказе 100 000 тг.</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('images/sprite.svg#fi-24-hours-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Поддержка 24/7</div>
                    <div class="block-features__subtitle">Мы готовы помочь вам</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('images/sprite.svg#fi-payment-security-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Оплата наличными</div>
                    <div class="block-features__subtitle">Банковский перевод</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ asset('images/sprite.svg#fi-tag-48') }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">Регулярные</div>
                    <div class="block-features__subtitle">Скидки до 30%</div>
                </div>
            </div>
        </div>
    </div>
</div>