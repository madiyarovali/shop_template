<div class="block block-banner">
    <div class="container">
        <a href="{{ $banner->link_href }}" class="block-banner__body">
            <div
                class="block-banner__image block-banner__image--desktop"
                style="background-image: url({{ asset("storage/$banner->full_path") }})">    
            </div>
            <div
                class="block-banner__image block-banner__image--mobile"
                style="background-image: url({{ asset("storage/$banner->mobile_path") }})">
            </div>
            <div class="block-banner__title">
                {{ $banner->title }}
            </div>
            <div class="block-banner__text">
                {{ $banner->subtitle }}
            </div>
            <div class="block-banner__button">
                <span class="btn btn-sm btn-primary">{{ $banner->link_text }}</span>
            </div>
        </a>
    </div>
</div>