@php
$fix_menu = isset($fix_menu) ? $fix_menu : 0;
$grand_parents = \App\Category::where('parent_id', null)->orderBy('id', 'DESC')->get();
@endphp
<div class="mobilemenu">
    <div class="mobilemenu__backdrop"></div>
    <div class="mobilemenu__body">
        <div class="mobilemenu__header">
            <div class="mobilemenu__title">Меню</div>
            <button type="button" class="mobilemenu__close">
                <svg width="20px" height="20px">
                    <use xlink:href="{{ asset('images/sprite.svg#cross-20') }}"></use>
                </svg>
            </button>
        </div>
        <div class="mobilemenu__content">
            <ul class="mobile-links mobile-links--level--0" data-collapse data-collapse-opened-class="mobile-links__item--open">
                @foreach (menu('main', '_json') as $item)
                    @if ($item->children->count() > 0)
                        <li class="mobile-links__item" data-collapse-item>
                            <div class="mobile-links__item-title">
                                <a href="{{ $item->link() }}" class="mobile-links__item-link">{{ $item->title }}</a>
                                <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                    <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                        <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-down-12x7') }}"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="mobile-links__item-sub-links" data-collapse-content>
                                <ul class="mobile-links mobile-links--level--1">
                                    @foreach ($item->children as $child)
                                    <li class="mobile-links__item" data-collapse-item>
                                        <div class="mobile-links__item-title">
                                            <a href="{{ $child->link() }}" class="mobile-links__item-link">{{ $child->title }}</a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @else
                        <li class="mobile-links__item">
                            <div class="mobile-links__item-title">
                                <a class="mobile-links__item-link" href="{{ $item->link() }}">
                                    {{ $item->title }}
                                </a>
                            </div>
                        </li>
                    @endif
                @endforeach
                @foreach($grand_parents as $grand_parent)
                <li class="mobile-links__item" data-collapse-item>
                    @if ($grand_parent->children->count() > 0)
                    <div class="mobile-links__item-title">
                        <a href="/category/{{ $grand_parent->id }}" class="mobile-links__item-link">{{ $grand_parent->name }} </a>
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                            <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-down-12x7') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                            @foreach ($grand_parent->children as $parent)
                            @if ($parent->children->count() > 0)
                            <li class="mobile-links__item" data-collapse-item>
                                <div class="mobile-links__item-title">
                                    <a href="/category/{{ $parent->id }}" class="mobile-links__item-link">{{ $parent->name }}</a>
                                    <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                        <svg class="mobile-links__item-arrow" width="12px" height="7px">
                                            <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-down-12x7') }}"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="mobile-links__item-sub-links" data-collapse-content>
                                    <ul class="mobile-links mobile-links--level--2">
                                        @foreach ($parent->children as $child)
                                        <li class="mobile-links__item" data-collapse-item>
                                            <div class="mobile-links__item-title">
                                                <a href="/category/{{ $child->id }}" class="mobile-links__item-link">
                                                    {{ $child->name }}
                                                </a>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>    
                            @else
                            <li class="mobile-links__item">
                                <div class="mobile-links__item-title">
                                    <a href="/category/{{ $parent->id }}" class="mobile-links__item-link">{{ $parent->name }}</a>
                                </div>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    @else
                    <div class="mobile-links__item-title">
                        <a href="/category/{{ $grand_parent->id }}" class="mobile-links__item-link">
                            {{ $grand_parent->name }}
                        </a>
                    </div>
                    @endif
                </li>
                @endforeach
                @if (!Auth::check())
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/login" class="mobile-links__item-link">
                            Войти
                        </a>
                    </div>
                </li>
                @else
                <li class="mobile-links__item" data-collapse-item>
                    <div class="mobile-links__item-title">
                        <a href="/account/dashboard" class="mobile-links__item-link">
                            Домой
                        </a>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>