@php
$fix_menu = isset($fix_menu) ? $fix_menu : 0;
$grand_parents = \App\Category::where('parent_id', null)->orderBy('id', 'DESC')->get();
@endphp
<div class="site-header__nav-panel">
    <!-- data-sticky-mode - one of [pullToShow, alwaysOnTop] -->
    <div class="nav-panel nav-panel--sticky" data-sticky-mode="pullToShow">
        <div class="nav-panel__container container">
            <div class="nav-panel__row">
                <div class="nav-panel__departments">
                    <!-- .departments -->
                    @if ($fix_menu)
                    <div class="departments departments--open departments--fixed" data-departments-fixed-by=".block-slideshow">
                    @else
                    <div class="departments" data-departments-fixed-by="">
                    @endif
                        <div class="departments__body">
                            <div class="departments__links-wrapper" style="">
                                <div class="departments__submenus-container"></div>
                                <ul class="departments__links">
                                    @foreach($grand_parents as $grand_parent)
                                    <li class="departments__item">
                                        @if ($grand_parent->children->count() > 0)
                                        <a class="departments__item-link" href="/category/{{ $grand_parent->id }}">
                                            {{ $grand_parent->name }} 
                                            <svg class="departments__item-arrow" width="6px" height="9px"><use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use></svg>
                                        </a>
                                        <div class="departments__submenu departments__submenu--type--megamenu departments__submenu--size--xl">
                                            <!-- .megamenu -->
                                            <div class="megamenu megamenu--departments">
                                                <div
                                                    class="megamenu__body"
                                                    @if (isset($grand_parent->image))
                                                    style="background-image: url({{ $grand_parent->image }});"
                                                    @endif
                                                    >
                                                    <div class="row">
                                                        @foreach ($grand_parent->children as $parent)
                                                        <div class="col-3">
                                                            <ul class="megamenu__links megamenu__links--level--0">
                                                                @if ($parent->children->count() > 0)
                                                                <li class="megamenu__item megamenu__item--with-submenu">
                                                                    <a href="/category/{{ $parent->id }}">{{ $parent->name }}</a>
                                                                    <ul class="megamenu__links megamenu__links--level--1">
                                                                        @foreach ($parent->children as $child)
                                                                        <li class="megamenu__item">
                                                                            <a href="/category/{{ $child->id }}">{{ $child->name }}</a>
                                                                        </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li class="megamenu__item">
                                                                    <a href="/category/{{ $parent->id }}">{{ $parent->name }}</a>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- .megamenu / end -->
                                        </div>
                                        @else
                                            <a class="departments__item-link" href="/category/{{ $grand_parent->id }}">{{ $grand_parent->name }}</a>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <button class="departments__button">
                            <svg class="departments__button-icon" width="18px" height="14px">
                                <use xlink:href="{{ asset('images/sprite.svg#menu-18x14') }}"></use>
                            </svg> Категории
                            <svg class="departments__button-arrow" width="9px" height="6px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-down-9x6') }}"></use>
                            </svg>
                        </button>
                    </div>
                    <!-- .departments / end -->
                </div>
                <!-- .nav-links -->
                <div class="nav-panel__nav-links nav-links">
                    <ul class="nav-links__list">
                        @foreach (menu('main', '_json') as $item)
                            @if ($item->children->count() > 0)
                                <li class="nav-links__item nav-links__item--has-submenu">
                                    <a class="nav-links__item-link" href="{{ $item->link() }}">
                                        <div class="nav-links__item-body">{{ $item->title }}
                                            <svg class="nav-links__item-arrow" width="9px" height="6px">
                                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-down-9x6') }}"></use>
                                            </svg>
                                        </div>
                                    </a>
                                    <div class="nav-links__submenu nav-links__submenu--type--menu" style="">
                                        <!-- .menu -->
                                        <div class="menu menu--layout--classic">
                                            <div class="menu__submenus-container"></div>
                                            <ul class="menu__list">
                                                @foreach ($item->children as $child)
                                                <li class="menu__item">
                                                    <div class="menu__item-submenu-offset"></div>
                                                    <a class="menu__item-link" href="{{ $child->link() }}">{{ $child->title }}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <!-- .menu / end -->
                                    </div>
                                </li>
                            @else
                                <li class="nav-links__item">
                                    <a class="nav-links__item-link" href="{{ $item->link() }}">
                                        <div class="nav-links__item-body">{{ $item->title }}</div>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <!-- .nav-links / end -->
                <div class="nav-panel__indicators">
                    <div class="indicator">
                        <a href="#" class="indicator__button open-wishlist">
                            <span class="indicator__area">
                                <svg width="20px" height="20px">
                                    <use xlink:href="{{ asset('images/sprite.svg#heart-20') }}"></use>
                                </svg> 
                                <span class="indicator__value count">{{ Cart::instance('wishlist')->count() }}</span>
                            </span>
                        </a>
                    </div>
                    <div class="indicator">
                        <a href="#" class="indicator__button open-cart">
                            <span class="indicator__area">
                                <svg width="20px" height="20px">
                                    <use xlink:href="{{ asset('images/sprite.svg#cart-20') }}"></use>
                                </svg> 
                                <span class="indicator__value count">{{ Cart::instance('cart')->count() }}</span>
                            </span>
                        </a>
                    </div>
                    <div class="indicator indicator--trigger--click"><a href="/account/dashboard" class="indicator__button"><span class="indicator__area"><svg width="20px" height="20px"><use xlink:href="{{ asset('images/sprite.svg#person-20') }}"></use></svg></span></a>
                        <div class="indicator__dropdown">
                            <div class="account-menu">
                                @if (!Auth::check())
                                <form class="account-menu__form" method="post" action="login">
                                    @csrf
                                    <div class="account-menu__form-title">Войдите в свой профиль</div>
                                    <div class="form-group">
                                        <label for="header-signin-email" class="sr-only">Email адрес</label>
                                        <input id="header-signin-email" type="email" class="form-control form-control-sm" placeholder="Email адрес">
                                    </div>
                                    <div class="form-group">
                                        <label for="header-signin-password" class="sr-only">Пароль</label>
                                        <div class="account-menu__form-forgot">
                                            <input id="header-signin-password" type="password" class="form-control form-control-sm" placeholder="Пароль"> <a href="" class="account-menu__form-forgot-link">Забыли?</a></div>
                                    </div>
                                    <div class="form-group account-menu__form-button">
                                        <button type="submit" class="btn btn-primary btn-sm">Войти</button>
                                    </div>
                                    <div class="account-menu__form-link"><a href="/register">Создать профиль</a></div>
                                </form>
                                <div class="account-menu__divider"></div>
                                @else
                                @php
                                $user = Auth::user();
                                @endphp
                                <a href="/account/dashboard" class="account-menu__user">
                                    <div class="account-menu__user-avatar">
                                        <img src="{{ asset("storage/$user->avatar") }}" alt="">
                                    </div>
                                    <div class="account-menu__user-info">
                                        <div class="account-menu__user-name">{{ $user->name }}</div>
                                        <div class="account-menu__user-email">{{ $user->email }}</div>
                                    </div>
                                </a>
                                <div class="account-menu__divider"></div>
                                <ul class="account-menu__links">
                                    <li><a href="/account/profile">Изменить профиль</a></li>
                                    <li><a href="/account/orders">История заказов</a></li>
                                    <li><a href="/account/addresses">Адреса</a></li>
                                    <li><a href="/account/password">Пароль</a></li>
                                </ul>
                                <div class="account-menu__divider"></div>
                                <ul class="account-menu__links">
                                    <li><a href="/logout">Выйти</a></li>
                                </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>