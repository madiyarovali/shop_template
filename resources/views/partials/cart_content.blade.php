@php
$items = Cart::instance('cart')->content();
@endphp
<div class="dropcart__products-list">
    @if (Cart::instance('cart')->count() > 0)
        @foreach ($items as $item)
            <div class="dropcart__product">
                <div class="product-image dropcart__product-image">
                    <a href="/product/{{ $item->id }}" class="product-image__body">
                        <img
                            class="product-image__img"
                            src="{{ asset("/storage/". $item->model->image) }}"
                            alt="{{ $item->model->name }}">
                    </a>
                </div>
                <div class="dropcart__product-info">
                    <div class="dropcart__product-name"><a href="/product/{{ $item->id }}">{{ $item->name }}</a></div>
                    <div class="dropcart__product-options">
                        {!! mb_substr(strip_tags($item->model->short_description), 0, 100) !!}...
                    </div>
                    <div class="dropcart__product-meta">
                        <span class="dropcart__product-quantity">
                            <button class="qty-changers subs" data-initial="{{ $item->qty }}">-</button>
                            <span class="current-qty">{{ $item->qty }}</span>
                            <button class="qty-changers add" data-initial="{{ $item->qty }}">+</button>
                        </span>
                         × 
                        <span class="dropcart__product-price">{{ $item->price }} ₸</span>
                        <button class="update-cart d-none cart-updater ml-2" row-id="{{ $item->rowId }}" qty="{{ $item->qty }}">Обновить корзину</button>
                    </div>
                </div>
                <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon remove-cart" row-id="{{ $item->rowId }}">
                    <svg width="10px" height="10px">
                        <use xlink:href="{{ asset('images/sprite.svg#cross-10') }}"></use>
                    </svg>
                </button>
            </div>
        @endforeach
    @else
        <p class="h3 text-muted text-center">
             Корзина пустая
        </p>
    @endif
</div>
@if (Cart::instance('cart')->count() > 0)
    <div class="dropcart__totals">
        <table>
            <tbody>
                <tr>
                    <th>Итог</th>
                    <td>{{ Cart::instance('cart')->subtotal() }} ₸</td>
                </tr>
            </tbody>
        </table>
    </div>
@endif
<div class="dropcart__buttons">
    <a class="btn btn-secondary" href="#" data-dismiss="modal" aria-label="Close">Закрыть</a> 
    @if (Cart::instance('cart')->count() > 0)
    <a class="btn btn-primary" href="/checkout">Оформить заказ</a>
    @endif
</div>