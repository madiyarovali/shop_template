<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">Свяжитесь с нами</h5>
                            <div class="footer-contacts__text">Наши операторы всегда готовы помочь вам</div>
                            <ul class="footer-contacts__contacts">
                                <li><i class="footer-contacts__icon fas fa-globe-americas"></i> {{ setting('site.address') }}</li>
                                <li><i class="footer-contacts__icon far fa-envelope"></i> <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></li>
                                <li><i class="footer-contacts__icon fas fa-mobile-alt"></i> <a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a></li>
                                <li><i class="footer-contacts__icon far fa-clock"></i> {{ setting('site.schedule') }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Информация</h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item">
                                    <a href="/page/about-us" class="footer-links__link">О нас</a>
                                </li>
                                <li class="footer-links__item">
                                    <a href="/page/contact-us" class="footer-links__link">Контакты</a>
                                </li>
                                <li class="footer-links__item">
                                    <a href="/page/terms" class="footer-links__link">Условия конфиденциальность</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Мой профиль</h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item"><a class="footer-links__link" href="/account/dashboard">Главная</a></a></li>
                                <li class="footer-links__item"><a class="footer-links__link" href="/account/profile">Изменить профиль</a></a></li>
                                <li class="footer-links__item"><a class="footer-links__link" href="/account/orders">История заказов</a></a></li>
                                <li class="footer-links__item"><a class="footer-links__link" href="/account/addresses">Адреса</a></a></li>
                                <li class="footer-links__item"><a class="footer-links__link" href="/account/password">Пароль</a></a></li>
                                <li class="footer-links__item"><a class="footer-links__link" href="/logout">Выйти</a></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4">
                        <div class="site-footer__widget footer-newsletter">
                            <h5 class="footer-links__title">Наше местоположение</h5>
                            <div style="position:relative;overflow:hidden;">
                                <a href="https://yandex.kz/maps/162/almaty/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Алматы</a><a href="https://yandex.kz/maps/162/almaty/house/Y08YfgBpTkMFQFppfX51eXpnbQ==/?ll=76.880420%2C43.248473&sctx=ZAAAAAgBEAAaKAoSCc%2BgoX%2BCPFNAEdDukGKAnkVAEhIJE432%2F38B1T8ROUKKKpF%2BxT8iBQABAgQFKAowADivj6Tnsv3Q2F1AogFIAVXNzMw%2BWABqAmt6cACdAc3MTD2gAQCoAQC9AeUP56HCAQvekOnqA8D6nOvKBA%3D%3D&sll=76.880420%2C43.248473&sspn=0.002564%2C0.001312&utm_medium=mapframe&utm_source=maps&z=19" style="color:#eee;font-size:12px;position:absolute;top:14px;">Улица Туркебаева, 96 — Яндекс.Карты</a><iframe src="https://yandex.kz/map-widget/v1/-/CKubMZNU" width="100%" height="200" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="totop">
            <div class="totop__body">
                <div class="totop__start"></div>
                <div class="totop__container container"></div>
                <div class="totop__end">
                    <button type="button" class="totop__button">
                        <svg width="13px" height="8px">
                            <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-up-13x8') }}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</footer>