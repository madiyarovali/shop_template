<div class="block block-brands" id="brands">
    <div class="container">
        <div class="block-brands__slider">
            <div class="owl-carousel">
                @foreach ($slider->images as $brand)
                <div class="block-brands__item">
                    <a href="{{ $brand->link_href }}"><img src="{{ asset("storage/$brand->full_path") }}" alt=""></a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>