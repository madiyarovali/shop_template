<div class="block block-products block-products--layout--large-first" data-mobile-grid-columns="2">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">{{ $slider->name }}</h3>
            <div class="block-header__divider"></div>
        </div>
        <div class="block-products__body">
            <div class="block-products__featured">
                @php
                $product = $slider->products()->first();
                @endphp
                <div class="block-products__featured-item">
                    @include('partials.product_card', ['product' => $product])
                </div>
            </div>
            <div class="block-products__list">
                @foreach ($slider->products as $product)
                <div class="block-products__list-item">
                    @include('partials.product_card', ['product' => $product])
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>