<div class="modal fade" id="address-modal" tabindex="-1" role="dialog" aria-labelledby="address-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="address-modal-label"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body address-html">
                <div class="card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12">
                                <form method="post">
                                    @csrf
                                    <input type="hidden" name="id" id="address-id">
                                    <div class="form-group">
                                        <label for="address-name">Название</label>
                                        <input type="text" class="form-control" id="address-name" placeholder="Домашний" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="address-country">Страна</label>
                                        <select id="address-country" class="form-control form-control-select2" name="country">
                                            <option>Выберите страну...</option>
                                            <option value="Казахстан">Казахстан</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="address-street-address">Адрес</label>
                                        <input type="text" class="form-control" id="address-street-address" placeholder="Улица Дом, Квартира" name="address">
                                    </div>
                                    <div class="form-group">
                                        <label for="address-city">Город</label>
                                        <input type="text" class="form-control" id="address-city" name="city" placeholder="Алматы">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="address-email">Email почта</label>
                                            <input type="email" class="form-control" id="address-email" placeholder="example@gmail.com" name="email">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="address-phone">Телефон</label>
                                            <input type="text" class="form-control" id="address-phone" placeholder="+7 (701) 234 56-78" name="phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <span class="form-check-input input-check">
                                                <span class="input-check__body">
                                                    <input class="input-check__input" type="checkbox" name="is_default" id="address-default" checked> 
                                                    <span class="input-check__box"></span>
                                                    <svg class="input-check__icon" width="9px" height="7px">
                                                        <use xlink:href="{{ asset('images/sprite.svg#check-9x7') }}"></use>
                                                    </svg>
                                                </span>
                                            </span>
                                            <label class="form-check-label" for="address-default">Использовать по умолчанию</label>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3 mb-0">
                                        <button class="btn btn-primary" type="submit">Сохранить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>