<div class="product-card product-card--hidden-actions">
    <a class="product-card__quickview" href="/product/{{ $product->id }}">
        <svg width="16px" height="16px">
            <use xlink:href="{{ asset('images/sprite.svg#quickview-16') }}"></use>
        </svg> 
        <span class="fake-svg-icon"></span>
    </a>
    <div class="product-card__badges-list">
        @foreach ($product->getBadges() as $lable => $text)
            <div class="product-card__badge product-card__badge--{{ $lable }}">{{ $text }}</div>
        @endforeach
    </div>
    <div class="product-card__image product-image">
        <a href="/product/{{ $product->id }}" class="product-image__body">
            <img class="product-image__img" src="{{ asset("storage/$product->image") }}" alt="">
        </a>
    </div>
    <div class="product-card__info">
        <div class="product-card__name">
            <a href="/product/{{ $product->id }}">
                {{ $product->name }}
            </a>
        </div>
        <div class="product-card__features-list">
            {!! $product->short_description !!}
        </div>
    </div>
    <div class="product-card__actions">
        <div class="product-card__prices">
            @if ($product->old_price !== null)
            <span class="product-card__new-price">
                {{ $product->new_price }} ₸
            </span> 
            <span class="product-card__old-price">
                {{ $product->old_price }} ₸
            </span>
            @else
                {{ $product->new_price }} ₸
            @endif
        </div>
        
        <div class="product-card__buttons">
            <button
                class="btn btn-primary product-card__addtocart add-cart"
                type="button"
                product-id="{{ $product->id }}"
                qty="1">
                В корзину
            </button>
            <button
                class="btn btn-secondary product-card__addtocart product-card__addtocart--list add-cart"
                type="button"
                product-id="{{ $product->id }}"
                qty="1">
                В корзину
            </button>
            <button
                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist add-wishlist"
                product-id="{{ $product->id }}"
                qty="1"
                type="button">
                <svg width="16px" height="16px">
                    <use xlink:href="{{ asset('images/sprite.svg#wishlist-16') }}"></use>
                </svg> 
                <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
            </button>
        </div>
    </div>
</div>