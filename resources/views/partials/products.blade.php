@foreach ($products as $product)
<div class="products-list__item">
    @include('partials.product_card', ['product' => $product])
</div>
@endforeach