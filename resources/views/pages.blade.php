@extends('layouts.app', ['title' => $page->title])
@section('content')
<div class="site__body">
    <div class="container">
        <div class="row">
            <div class="col-12 py-5">
                {!! $page->text !!}
            </div>
        </div>
    </div>
</div>
@endsection