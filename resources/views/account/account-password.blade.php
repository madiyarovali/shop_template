@extends('layouts.app')
@section('content')
<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item"><a href="/account/dashboard">Мой аккаунт</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Изменить пароль</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Изменить пароль</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 d-flex">
                    <div class="account-nav flex-grow-1">
                        <h4 class="account-nav__title">Навигация</h4>
                        <ul>
                            <li class="account-nav__item"><a href="/account/dashboard">Главная</a></li>
                            <li class="account-nav__item"><a href="/account/profile">Изменить профайл</a></li>
                            <li class="account-nav__item"><a href="/account/orders">История заказа</a></li>
                            <li class="account-nav__item"><a href="/account/addresses">Мои адреса</a></li>
                            <li class="account-nav__item account-nav__item--active"><a href="/account/password">Изменить пароль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                    <div class="card">
                        <div class="card-header">
                            <h5>Изменить пароль</h5>
                        </div>
                        <div class="card-divider"></div>
                        <div class="card-body">
                            <div class="row no-gutters">
                                <div class="col-12 col-lg-7 col-xl-6">
                                    <form action="/account/reset/password" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="password-current">Текущий пароль</label>
                                            <input type="password" class="form-control" id="password-current" placeholder="********" name="current_password">
                                        </div>
                                        <div class="form-group">
                                            <label for="password-new">Новый пароль</label>
                                            <input type="password" class="form-control" id="password-new" placeholder="**********" name="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="password-confirm">Повторите новый пароль</label>
                                            <input type="password" class="form-control" id="password-confirm" placeholder="**********" name="password_confirmation">
                                        </div>
                                        <div class="form-group mt-5 mb-0">
                                            <button class="btn btn-primary" type="submit">Изменить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection