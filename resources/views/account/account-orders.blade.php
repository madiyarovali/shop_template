@extends('layouts.app')
@section('content')
@include('partials.order_modal')
<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item"><a href="/account/dashboard">Мой аккаунт</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Мои заказы</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Мои заказы</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 d-flex">
                    <div class="account-nav flex-grow-1">
                        <h4 class="account-nav__title">Навигация</h4>
                        <ul>
                            <li class="account-nav__item"><a href="/account/dashboard">Главная</a></li>
                            <li class="account-nav__item"><a href="/account/profile">Изменить профайл</a></li>
                            <li class="account-nav__item account-nav__item--active"><a href="/account/orders">История заказа</a></li>
                            <li class="account-nav__item"><a href="/account/addresses">Мои адреса</a></li>
                            <li class="account-nav__item"><a href="/account/password">Изменить пароль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                    <div class="card">
                        <div class="card-header">
                            <h5>Заказы</h5>
                        </div>
                        <div class="card-divider"></div>
                        <div class="card-table">
                            <div class="table-responsive-sm">
                                @if ($orders->count() > 0)
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Заказ №</th>
                                            <th>Дата</th>
                                            <th>Статус</th>
                                            <th>Итог</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                        <tr>
                                            <td>
                                                <a  href="#"
                                                    class="order-modal-opener"
                                                    order-id="{{ $order->id }}"
                                                    data="{{ json_encode($order->orderItems) }}">
                                                    #{{ $order->id }}
                                                </a>
                                            </td>
                                            <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                            <td>{{ $order->status }}</td>
                                            <td>{{ $order->total }} ₸ из {{ $order->orderItems->count() }} предмета(ов)</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <p class="h3 text-center text-muted p-2">
                                    Нет заказов
                                </p>
                                @endif
                            </div>
                        </div>
                        <div class="card-divider"></div>
                        @if ($orders->count() > 0)
                        <div class="card-footer">
                            {{ $orders->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
        