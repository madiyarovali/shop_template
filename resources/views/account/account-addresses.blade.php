@extends('layouts.app')
@section('content')
@include('partials.address_modal')
<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item"><a href="/account/dashboard">Мой аккаунт</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Мои адреса</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Мои адреса</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 d-flex">
                    <div class="account-nav flex-grow-1">
                        <h4 class="account-nav__title">Навигация</h4>
                        <ul>
                            <li class="account-nav__item"><a href="/account/dashboard">Главная</a></li>
                            <li class="account-nav__item"><a href="/account/profile">Изменить профайл</a></li>
                            <li class="account-nav__item"><a href="/account/orders">История заказа</a></li>
                            <li class="account-nav__item account-nav__item--active"><a href="/account/addresses">Мои адреса</a></li>
                            <li class="account-nav__item"><a href="/account/password">Изменить пароль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                    <div class="addresses-list">
                        <a href="#" class="addresses-list__item addresses-list__item--new add-address-toggler">
                            <div class="addresses-list__plus"></div>
                            <div class="btn btn-secondary btn-sm add-address-toggler">Добавить адрес</div>
                        </a>
                        @foreach ($addresses as $address)
                            <div class="addresses-list__divider"></div>
                            <div class="addresses-list__item card address-card">
                                @if ($address->is_default)
                                <div class="address-card__badge">Адрес по умолчанию</div>
                                @endif
                                <div class="address-card__body">
                                    <div class="address-card__name">{{ $address->name }}</div>
                                    <div class="address-card__row">
                                        {{ $address->country }}<br>
                                        {{ $address->city }}<br>
                                        {{ $address->address }}    
                                    </div>
                                    @if ($address->phone) 
                                    <div class="address-card__row">
                                        <div class="address-card__row-title">Номер телефона</div>
                                        <div class="address-card__row-content">{{ $address->phone }}</div>
                                    </div>
                                    @endif
                                    @if ($address->email) 
                                    <div class="address-card__row">
                                        <div class="address-card__row-title">Почта</div>
                                        <div class="address-card__row-content">{{ $address->email }}</div>
                                    </div>
                                    @endif
                                    <div class="address-card__footer">
                                        <a  href="#"
                                            class="update-address-toggler"
                                            data="{{ json_encode($address) }}">Изменить</a>&nbsp;&nbsp; <a href="#" class="remove-address">Удалить</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="addresses-list__divider"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection