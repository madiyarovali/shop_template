@extends('layouts.app')
@section('content')
@include('partials.order_modal')
<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Мой аккаунт</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Мой аккаунт</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 d-flex">
                    <div class="account-nav flex-grow-1">
                        <h4 class="account-nav__title">Навигация</h4>
                        <ul>
                            <li class="account-nav__item account-nav__item--active"><a href="/account/dashboard">Главная</a></li>
                            <li class="account-nav__item"><a href="/account/profile">Изменить профайл</a></li>
                            <li class="account-nav__item"><a href="/account/orders">История заказа</a></li>
                            <li class="account-nav__item"><a href="/account/addresses">Мои адреса</a></li>
                            <li class="account-nav__item"><a href="/account/password">Изменить пароль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                    <div class="dashboard">
                        <div class="dashboard__profile card profile-card">
                            <div class="card-body profile-card__body">
                                <div class="profile-card__avatar"><img src="{{ asset("storage/$user->avatar") }}" alt="{{ $user->name }}"></div>
                                <div class="profile-card__name">{{ $user->name }}</div>
                                <div class="profile-card__email">{{ $user->email }}</div>
                                <div class="profile-card__edit"><a href="/account/profile" class="btn btn-secondary btn-sm">Изменить профайл</a></div>
                            </div>
                        </div>
                        <div class="dashboard__address card address-card address-card--featured">
                            <div class="address-card__badge">Адрес по умолчанию</div>
                            <div class="address-card__body">
                                @if ($default_address->count() > 0)
                                <div class="address-card__name">{{ $default_address->first()->name }}</div>
                                <div class="address-card__row">
                                    {{ $default_address->first()->country }}<br>
                                    {{ $default_address->first()->city }}<br>
                                    {{ $default_address->first()->address }}
                                </div>
                                @if ($default_address->first()->phone) 
                                <div class="address-card__row">
                                    <div class="address-card__row-title">Номер телефона</div>
                                    <div class="address-card__row-content">{{ $default_address->first()->phone }}</div>
                                </div>
                                @endif
                                @if ($default_address->first()->email) 
                                <div class="address-card__row">
                                    <div class="address-card__row-title">Почта</div>
                                    <div class="address-card__row-content">{{ $default_address->first()->email }}</div>
                                </div>
                                @endif
                                <div class="address-card__footer"><a href="/account/addresses#{{ $default_address->first()->id }}">Изменить адрес</a></div>
                                @else
                                <div class="address-card__row">
                                    <div class="address-card__row-title">
                                        <p class="h3 text-center">
                                            Нет адреса
                                        </p>
                                    </div>
                                </div>
                                <div class="address-card__footer text-center"><a href="/account/addresses">Добавить адрес <span class="address-add-btn">+</span></a></div>
                                @endif
                            </div>
                        </div>
                        <div class="dashboard__orders card">
                            <div class="card-header">
                                <h5>Последние заказы</h5></div>
                            <div class="card-divider"></div>
                            <div class="card-table">
                                <div class="table-responsive-sm">
                                    @if ($recently_orders->count() > 0)
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Заказ №</th>
                                                <th>Дата</th>
                                                <th>Статус</th>
                                                <th>Итог</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($recently_orders as $order)
                                            <tr>
                                                <td><a href="#" class="order-modal-opener" order-id="{{ $order->id }}" data="{{ json_encode($order->orderItems) }}">#{{ $order->id }}</a></td>
                                                <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                                <td>{{ $order->status }}</td>
                                                <td>{{ $order->total }} ₸ из {{ $order->orderItems->count() }} предмета(ов)</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <p class="h3 text-center text-muted p-2">
                                        Нет заказов
                                    </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection