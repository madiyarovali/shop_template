@extends('layouts.app')
@section('content')
<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
            <div class="page-header__breadcrumb">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Главная</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item"><a href="/account/dashboard">Мой аккаунт</a>
                            <svg class="breadcrumb-arrow" width="6px" height="9px">
                                <use xlink:href="{{ asset('images/sprite.svg#arrow-rounded-right-6x9') }}"></use>
                            </svg>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Изменить профайл</li>
                    </ol>
                </nav>
            </div>
            <div class="page-header__title">
                <h1>Изменить профайл</h1>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 d-flex">
                    <div class="account-nav flex-grow-1">
                        <h4 class="account-nav__title">Навигация</h4>
                        <ul>
                            <li class="account-nav__item"><a href="/account/dashboard">Главная</a></li>
                            <li class="account-nav__item account-nav__item--active"><a href="/account/profile">Изменить профайл</a></li>
                            <li class="account-nav__item"><a href="/account/orders">История заказа</a></li>
                            <li class="account-nav__item"><a href="/account/addresses">Мои адреса</a></li>
                            <li class="account-nav__item"><a href="/account/password">Изменить пароль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                    <div class="card">
                        <div class="card-header">
                            <h5>Изменить</h5></div>
                        <div class="card-divider"></div>
                        <div class="card-body">
                            <div class="row no-gutters">
                                <div class="col-12 col-lg-7 col-xl-6">
                                    <form action="/account/update/profile" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="profile-first-name">Ваше имя</label>
                                            <input
                                                required
                                                type="text"
                                                class="form-control"
                                                id="profile-first-name"
                                                placeholder="Ваше имя"
                                                name="name"
                                                value="{{ $user->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="profile-email">Email почта</label>
                                            <input
                                                required
                                                type="email"
                                                class="form-control"
                                                id="profile-email"
                                                placeholder="Email почта"
                                                name="email"
                                                value="{{ $user->email }}">
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="validated-custom-file" name="avatar">
                                            <label class="custom-file-label" for="validated-custom-file">Выбрать аватар...</label>
                                        </div>
                                        <div class="form-group mt-5 mb-0">
                                            <button class="btn btn-primary" type="submit">Сохранить</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-lg-5 col-xl-6">
                                    <div class="text-center mx-auto" style="width: 200px; height: 200px; overflow: hidden;">
                                        <img src="{{ asset("storage/$user->avatar") }}" class="img-fluid rounded-circle">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection