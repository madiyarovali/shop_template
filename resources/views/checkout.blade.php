@extends('layouts.app')
@section('content')
<div class="site__body">
    @include('partials.breadcrumbs', ['name' => 'Оформить заказ', 'ancestors' => []])
    <form id="checkout-form" action="/make/order" method="post">
        <div class="checkout block">
            <div class="container">
                <div class="row">
                    @if (Auth::guest())
                    <div class="col-12 mb-3">
                        <div class="alert alert-lg alert-primary">У вас есть аккаунт? <a href="/login">Войти</a></div>
                    </div>
                    @endif
                    <div class="col-12 col-lg-6 col-xl-7">
                        <div class="card mb-lg-0">
                            <div class="card-body">
                                <h3 class="card-title">Информация о покупателе</h3>
                                <div class="form-row">
                                    @csrf
                                    <div class="form-group col-md-6">
                                        <label for="checkout-first-name">Имя</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="first_name"
                                            id="checkout-first-name"
                                            placeholder="Имя"
                                            value="{{ Auth::guest() ? '' : Auth::user()->name }}"
                                            required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="checkout-last-name">Фамилия</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="last_name"
                                            id="checkout-last-name"
                                            placeholder="Фамилия"
                                            required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="checkout-email">Email aдрес</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            name="email"
                                            id="checkout-email"
                                            value="{{ Auth::guest() ? '' : Auth::user()->email }}"
                                            placeholder="Email aдрес">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="checkout-phone">Телефон</label>
                                        <input type="text" class="form-control" name="phone" id="checkout-phone" placeholder="Телефон" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <span class="form-check-input input-check">
                                            <span class="input-check__body">
                                                <input class="input-check__input" type="checkbox" name="pickup" id="checkout-pickup" checked> 
                                                <span class="input-check__box"></span>
                                                <svg class="input-check__icon" width="9px" height="7px">
                                                    <use xlink:href="images/sprite.svg#check-9x7"></use>
                                                </svg>
                                            </span>
                                        </span>
                                        <label class="form-check-label" for="checkout-pickup">Самовывоз</label>
                                    </div>
                                </div>

                                <div class="d-none delivery-information">
                                    <div class="form-group">
                                        <label for="checkout-region">Область / Город</label>
                                        <select name="region" id="checkout-region" class="form-control form-control-select2">
                                            <option disabled selected>Выберите вариант</option>
                                            @foreach ($options as $option => $activity)
                                                @if ($activity)
                                                    <option value="{{ $option }}">{{ $option }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-street-address">Улица</label>
                                        <input type="text" class="form-control" name="street" id="checkout-street-address" placeholder="Улица">
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-city">Город</label>
                                        <input type="text" class="form-control" name="city" id="checkout-city">
                                    </div>
                                    <div class="form-group">
                                        <label for="checkout-postcode">ZIP код</label>
                                        <input type="text" class="form-control" name="zip" id="checkout-postcode">
                                    </div>
                                </div>
                                <div class="pickup-information">
                                    <div class="form-group">
                                        <label for="checkout-pickup-info">Наш адрес</label>
                                        <input type="text" class="form-control" id="checkout-pickup-info" readonly value="{{ setting('site.address') }}">
                                        <a href="{{ setting('site.2gis') }}" target="_blank">Показать в 2Gis</a>
                                    </div>
                                </div>

                            </div>
                            <div class="card-divider"></div>
                            <div class="card-body">
                                <h3 class="card-title">Другие детали доставки</h3>
                                <div class="form-group">
                                    <label for="checkout-comment">Комментрии к заказу <span class="text-muted">(необязательно)</span></label>
                                    <textarea name="order_note" id="checkout-comment" class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-5 mt-4 mt-lg-0">
                        <div class="card mb-0">
                            <div class="card-body">
                                <h3 class="card-title">Ваш заказ</h3>
                                <table class="checkout__totals">
                                    <thead class="checkout__totals-header">
                                        <tr>
                                            <th>Наименование</th>
                                            <th>Сумма</th>
                                        </tr>
                                    </thead>
                                    <tbody class="checkout__totals-products checkout-html">
                                        @include('partials.checkout_content')
                                    </tbody>
                                    <tfoot class="checkout__totals-footer">
                                        <tr>
                                            <th>Итог</th>
                                            <td><span class="total">{{ Cart::instance('cart')->subtotal }}</span> ₸</td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="payment-methods">
                                    <ul class="payment-methods__list">
                                        <li class="payment-methods__item payment-methods__item--active">
                                            <label class="payment-methods__item-header">
                                                <span class="payment-methods__item-radio input-radio">
                                                    <span class="input-radio__body">
                                                        <input class="input-radio__input" name="checkout_payment_method" type="radio" checked="checked"> 
                                                        <span class="input-radio__circle"></span>
                                                    </span>
                                                </span>
                                                <span class="payment-methods__item-title">Оплата через банк</span>
                                            </label>
                                            <div class="payment-methods__item-container">
                                                <div class="payment-methods__item-description text-muted">
                                                    Мы вышлем Вам счет на оплату, после подтверждения оплаты мы совершим доставку
                                                </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <div class="checkout__agree form-group">
                                    <div class="form-check">
                                        <span class="form-check-input input-check">
                                            <span class="input-check__body">
                                                <input class="input-check__input" type="checkbox" id="checkout-terms" required> 
                                                <span class="input-check__box"></span>
                                                <svg class="input-check__icon" width="9px" height="7px">
                                                    <use xlink:href="images/sprite.svg#check-9x7"></use>
                                                </svg>
                                            </span>
                                        </span>
                                        <label class="form-check-label" for="checkout-terms">Я прочитал и согласен с <a target="_blank" href="terms-and-conditions">условиями</a>*</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-xl btn-block">Сделать заказ</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection