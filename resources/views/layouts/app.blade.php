<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? setting('site.title') }}</title>
    <meta name="description" content="{{ setting('site.description') }}">
    <link rel="icon" type="image/png" href="{{ asset("storage/". setting('site.logo')) }}">
    <!-- fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:400,500|Rubik+Mono+One|Roboto:400,400i,500,500i,700,700i&display=swap">
    <!-- css -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/photoswipe/photoswipe.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/photoswipe/default-skin/default-skin.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <!-- font - fontawesome -->
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <!-- font - stroyka -->
    <link rel="stylesheet" href="{{ asset('fonts/stroyka/stroyka.css') }}">
</head>

<body>
    <div class="site">
        @include('partials.header_mobile')
        @include('partials.header_desktop')
        @yield('content')
        @include('partials.footer')
    </div>
    
    @include('partials.quick_view')
    @include('partials.cart_modal')
    @include('partials.wishlist_modal')
    @include('partials.mobile_menu')
    @include('partials.photoswipe')
    <!-- js -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/nouislider/nouislider.min.js') }}"></script>
    <script src="{{ asset('vendor/photoswipe/photoswipe.min.js') }}"></script>
    <script src="{{ asset('vendor/photoswipe/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('js/notify.js') }}"></script>
    <script src="{{ asset('js/alert.js') }}"></script>
    <script src="{{ asset('js/number.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/header.js') }}"></script>
    <script src="{{ asset('js/cart.js') }}"></script>
    <script src="{{ asset('js/wishlist.js') }}"></script>
    <script src="{{ asset('js/requests.js') }}"></script>
    <script src="{{ asset('js/toggler.js') }}"></script>
    <script src="{{ asset('js/other.js') }}"></script>
    <script src="{{ asset('vendor/svg4everybody/svg4everybody.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            var on_add_cart_text = '{!! add_cart_text() !!}';
            var on_update_cart_text = '{!! update_cart_text() !!}';
            var on_delete_cart_text = '{!! delete_cart_text() !!}';
            init_cart(on_add_cart_text, on_update_cart_text, on_delete_cart_text);

            var on_add_wishlist_text = '{!! add_wishlist_text() !!}';
            var on_delete_wishlist_text = '{!! delete_wishlist_text() !!}';
            init_wishlist(on_add_cart_text, on_delete_cart_text);
        });
        svg4everybody();
    </script>
    @if (isset($message) and $message != '')
    <script>
        Swal.fire({
            // title: 'Спасибо за заказ',
            html: '{{ session("message") }}',
            icon: 'success',
            confirmButtonText: 'Ok'
        })
    </script>
    @endif
</body>

</html>